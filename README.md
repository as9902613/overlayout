# Qt OverLayout

A [Qt](https://qt.io) layout overlay widget for anchored (Top, Right, Bottom and Left) widgets. When mouse enter parent widget, the layout show (Top, Right, Bottom and Left) widgets show with cool animation. When mouse leave parent widget, hide them with revert animation.

Read [the doc](./html/docs/index.html) for more details about the Overlayout methods.

## Install

Open a shell in your Qt project :

```sh
mkdir vendor
cd vendor
git clone https://gitlab.com/GuilleW/overlayout.git

```

## Example


```cpp
#include "vendor/overlayout/src/overlayout.h"

  // Top Widget
  QWidget *top = new QWidget();
  top->setStyleSheet("max-height:100px; background-color: rgba(200,0,0,0.25);");

  // Right Widget
  QWidget *right = new QWidget();
  right->setStyleSheet("max-width:150px; background-color: rgba(0,0,200,0.25);");

  // Bottom Widget
  QWidget *bottom = new QWidget();
  bottom->setStyleSheet("max-height:100px; background-color: rgba(200,0,0,0.25);");

  // Left Widget
  QWidget *left = new QWidget();
  left->setStyleSheet("max-width:150px; background-color: rgba(0,0,200,0.25);");


  // Layout
  OverLayout layout(this);
  layout.show();

  // Add widgets to layouts
  layout.setTopWidget(top, OverLayout::Anchor::Both);
  layout.setRightWidget(right, OverLayout::Anchor::None);
  layout.setBottomWidget(bottom, OverLayout::Anchor::Both);
  layout.setLeftWidget(left, OverLayout::Anchor::None);

  // Use default animation,
  // or create your own layout Animation (like below)
  QParallelAnimationGroup *animV = new QParallelAnimationGroup(&layout);
  animV->addAnimation(layout.topAnimation(250));
  animV->addAnimation(layout.bottomAnimation(250));

  QParallelAnimationGroup *animH = new QParallelAnimationGroup(&layout);
  animH->addAnimation(layout.leftAnimation(250));
  animH->addAnimation(layout.rightAnimation(250));

  QSequentialAnimationGroup *animation = new QSequentialAnimationGroup(&layout);
  animation->addAnimation(animV);
  animation->addAnimation(animH);

  layout.setAnimation(animation);

```

## Contribute
All contributions, code, feedback and strategic advice, are welcome. If you have a question you can contact me directly via email or simply [open an issue](https://gitlab.com/GuilleW/overlayout/issues/new) on the repository. I'm also always happy for people helping me test new features.
